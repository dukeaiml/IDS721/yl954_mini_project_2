# yl954_mini_project_2

This is a simple AWS Lambda function that processes data.

## Author

Yuanzhi (Matthew) Lou

## Demo

[Live Preview](https://w72w6izqkb.execute-api.us-east-1.amazonaws.com/prod/mini2)

## Lambda Functionality

1. This function connects the S3 bucket named mini2bucket.
2. This function sorts interger lists
3. There are two parameters you can set: ```order(default:a)``` and ```list(default:[])```
4. For ```order```, ascending when its value is ```a``` and descending with all other values
5. For ```list```, each number is delimited by comma, and spaces have no effect on the result
6. Example:
```
https://w72w6izqkb.execute-api.us-east-1.amazonaws.com/prod/mini2
```

## Data Processing

1. This function convert ```list``` from ```String``` to ```Vec<i32>```. When the conversion fails, a empty vector will be returned
2. After conversion, the converted data will be sorted in ascending or descending order
3. Then, Both the sorted data and original data will be outputed

## API Gateway Integration

An API Gateway is created and this lambda function is integrated into the API Gateway. The stage to which this lambda function attached is named ```prod```, and the route is named ```/mini2```, which uses the ```GET``` method.

## S3 Bucket Setup
A S3 Bucket named mini2bucket is created, and a file named mini2data.csv which contains ```order``` and ```list``` is uploaded to the Bucket (Note: add permission ```AmazonS3FullAccess``` to the role who excutes this lambda function)

### mini2data.csv
```
order,list
a,"[1, 2, 3, 4, 55, 6, 34, 23, 56, 623, 23, 54, 44, 55, 77]"
d,"[1, 2, 3, 4, 55, 6, 34, 23, 56, 623, 23, 54, 44, 55, 77]"
d,"[a, b, c, 2, 4, 2]"
```

## Install

### Install Rust

1. ```sudo apt install curl```
2. ```curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh```

### Install Cargo Lambda

1. ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
2. ```(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/lyuanzhi/.bashrc```
3. ```eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"```
4. ```brew tap cargo-lambda/cargo-lambda```
5. ```brew install cargo-lambda```

### Basic Commands

1. ```cargo lambda new projectName```
2. ```cargo lambda watch```
3. ```cargo lambda build --release```

## Deploy

1. Create a AWS account
2. Go to IAM service, create access key and save the ```access key ID``` and ```secret access key```
3. Create a new role with the policy of ```AWSLambda_FullAccess```
4. In your VM, create the file ```~/.aws/credentials```, and add the following content

```
[default]
aws_access_key_id = your_access_key_ID
aws_secret_access_key = your_secret_access_key
```

5. Under your project: ```cargo lambda deploy --region your_region --iam-role arn:aws:iam::your_account_ID:role/your_role_name```
6. Then you can find the lambda function in your AWS account


